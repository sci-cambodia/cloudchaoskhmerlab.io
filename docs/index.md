# Cloud Chaos Khmer Game

It is a mobile game for iOS and Android.

Someone’s spreading harmful content on the Internet! Help Oun Pich, Oun Meas, and Zion clean it up before it’s too late!

Test your wits against 20+ puzzles across three different worlds! Push blocks, jump pits, evade enemies and gather all the pieces you need to unlock the exit and escape. And make sure to watch the web series to get all the secret game codes! You will also learn to protect yourself on Online Safety!

It is created in partnership with the Family Care First|REACT project under Save the Children Cambodia, NCMEC, Crystal Fish Games, Tiny Monster Studios, and Saigasound LLC.

This app does not collect, use, or disclose personal information. The only stored user data is a record of in-game progress.


- Save the Children Cambodia Facebook Page: [https://web.facebook.com/SavetheChildreninCambodia](https://web.facebook.com/SavetheChildreninCambodia)
- Family Care First|REACT Website : [https://www.fcf-react.org/](https://www.fcf-react.org/)
- Family Care First|REACT Website: [https://www.fcf-react.org/](https://www.fcf-react.org/)
- Family Care First|REACT Facebook Page: [https://web.facebook.com/FCFREACT](https://web.facebook.com/FCFREACT)

- Game Developer Website: [https://netsmartzkids.org/games](https://netsmartzkids.org/games)
- Support: [https://www.missingkids.org/footer/contactus](https://www.missingkids.org/footer/contactus)
- Privacy Policy: [https://www.missingkids.org/footer/legalinformation](https://www.missingkids.org/footer/legalinformation)

- [Cloud Chaos Khmer - Privacy](privacy.md)

- [Cloud Chaos Khmer - Term & Condition](term_condition.md)
